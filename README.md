# Eclipse-CDT on Docker

This repository is made for dockerizing Eclipse-CDT on a Linux machine. It's tested with Ubuntu 18.04. 

## Building the Docker-Image

The Docker image can be built with the command: 
    
    docker build -t eclipse_cdt

## Starting the Docker-Contaier

The Docker container can be started with the command: 
    
    docker run -ti --rm -e DISPLAY=$DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix eclipse_cdt

## Using Docker-Compose

The Docker container can be started via compose file and docker-compose up:       
    
    docker-compose up

The workspace repository needs to be changed or removed: 

    volumes:
      - /tmp/.X11-unix:/tmp/.X11-unix
      - /workspace:/workspace <-- here