FROM ubuntu:18.04

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    curl \
    g++ \
    gcc \
    make \   
    openjdk-11-jre \
    openjdk-11-jdk \ 
    sudo \
    tar \
    wget \
    libgtk-3-dev \
    gdb \
    libboost-all-dev

# Downloading Eclipse 
RUN wget -O eclipse.tar.gz http://ftp-stud.fht-esslingen.de/pub/Mirrors/eclipse/technology/epp/downloads/release/2019-09/M1/eclipse-cpp-2019-09-M1-linux-gtk-x86_64.tar.gz
RUN tar xfvz eclipse.tar.gz

RUN export uid=1000 gid=1000 && \
    mkdir -p /home/developer && \
    echo "developer:x:${uid}:${gid}:Developer,,,:/home/developer:/bin/bash" >> /etc/passwd && \
    echo "developer:x:${uid}:" >> /etc/group && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown ${uid}:${gid} -R /home/developer

USER developer
ENV HOME /home/developer

CMD /eclipse/eclipse
